package com.service;

//import java.util.List;
import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;

import com.model.User;

public interface UserDetailsService {
//	Iterable<User> findAll();

//    List<User> search(String term);

    Optional<User> findOne(int id);

    void save(User user);
    public UserDetails loadByUsername(String username);

}
