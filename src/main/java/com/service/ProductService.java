package com.service;

import java.util.List;
import java.util.Optional;

import com.model.Product;

public interface ProductService {
	Iterable<Product> findAll();

    List<Product> search(String term);

    Optional<Product> findOne(Long id);

    void save(Product product);

    void delete(Long id);
}
