package com.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;
import com.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	ProductRepository productrepository;
	
	@Override
	public Iterable<Product> findAll() {
		return productrepository.findAll();
	}

	@Override
	public List<Product> search(String term){
		return productrepository.findByProductNameContaining(term);
	}

	@Override
	public Optional<Product> findOne(Long id) {
		return productrepository.findById(id);
	}

	@Override
	public void save(Product product) {
		productrepository.save(product);
		
	}

	@Override
	public void delete(Long id) {
		productrepository.deleteById(id);;
	}
	
}
