package com.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;


@Entity
@Table(name = "product")
public class Product {
	@Transient
	private DecimalFormat df = new DecimalFormat("#,###.###");
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long productId;
	@NotEmpty
	private String productName;
	@NotEmpty
	private String price;
//	@Transient
//	private String pricePro;
//	@ManyToOne
//	@JoinColumn(name = "categoryId")
//	private Category cat;
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getPrice() {
		return price == null ? null : df.format(new Double(price)); 
	}
	public void setPrice(String price) {
		this.price = (price == "" ? null : price);
	}

//	public Category getCat() {
//		return cat;
//	}
//
//	public void setCat(Category cat) {
//		this.cat = cat;
//	}

	

	

}
