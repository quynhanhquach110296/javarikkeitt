package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
	//@Query("select p from Product p  where (p.productName like %:productName% or :productName is null)")
	List<Product> findByProductNameContaining(@Param("productName") String productName);
}