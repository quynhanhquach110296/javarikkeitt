package com.repository;

import org.springframework.data.repository.CrudRepository;

import com.model.Role;

public interface RoleRepository extends CrudRepository<Role, Integer> {

    Role findByName(String name);

}
