package com.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

@SuppressWarnings("deprecation")
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

//	@Autowired
//	private ThymeleafProperties properties;
//
//	@Value("${spring.thymeleaf.templates_root:}")
//	private String templatesRoot;
//
//	@Bean
//	public ITemplateResolver defaultTemplateResolver() {
//
//		FileTemplateResolver resolver = new FileTemplateResolver();
//		resolver.setSuffix(properties.getSuffix());
//		resolver.setPrefix(templatesRoot);
//		resolver.setTemplateMode(properties.getMode());
//		resolver.setCacheable(properties.isCache());
//		return resolver;
//	}
//
//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//
//		registry.addResourceHandler("/webjars/**", "/static/img/**", "/static/css/**", "/static/js/**")
//				.addResourceLocations("classpath:/META-INF/resources/webjars/", "classpath:/static/img/",
//						"classpath:/static/css/", "classpath:/static/js/");
//		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
//	}
	
//	@Bean
//    public MessageSource messageSource() {
//        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
//        messageSource.addBasenames("classpath:messages");
//        messageSource.setDefaultEncoding("UTF-8");
//        return messageSource;
//    }

}