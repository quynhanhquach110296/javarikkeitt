package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;

import com.model.Product;
import com.service.ProductService;

@Controller
@RequestMapping("/")
public class HomeController {

	@Autowired
	ProductService productService;
	
//	Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//
//	if (principal instanceof UserDetails) {
//	    String username = ((UserDetails) principal).getUsername();
//	} else {
//	    String username = principal.toString();
//	}
	
	
//	@GetMapping
//	public String home(Model model) {
//		model.addAttribute("msg", "Quynh Anh");
//		return "index";
//	}
	
	
	@GetMapping("/")
    public String index() {
        return "index";
    }


    @GetMapping("/403")
    public String accessDenied() {
        return "403";
    }

    @GetMapping("/login") 
    public String getLogin() {
        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        org.springframework.security.core.Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }
    
	@GetMapping("/admin")
	public String admin(Model model) {
		model.addAttribute("products", productService.findAll());
		return "admin";
	}
	
	@GetMapping("/admin/search")
	public String search(@RequestParam("term") String term, Model model) {
	    if (StringUtils.isEmpty(term)) {
	        return "redirect:/admin";
	    }

	    model.addAttribute("products", productService.search(term));
	    return "admin";
	}
	
	@GetMapping("/admin/add")
	public String addProduct(Model model) {
		model.addAttribute("product", new Product());
		return "form";
	}
	
	@PostMapping("/admin/save")
	public String save(@Valid Product product, BindingResult result, RedirectAttributes redirect) {
	    if (result.hasErrors()) {
	        return "form";
	    }
	    productService.save(product);
	    redirect.addFlashAttribute("successMessage", "Luu thanh cong!");
	    return "redirect:/admin";
	}
	
	@GetMapping("/admin/{productId}/edit")
	public String edit(@PathVariable("productId") Long productId, Model model) {
	    model.addAttribute("product", productService.findOne(productId));
	    return "form";
	}
	
	@GetMapping("/admin/{productId}/delete")
	public String delete(@PathVariable Long productId, RedirectAttributes redirect) {
	    productService.delete(productId);
	    redirect.addFlashAttribute("successMessage", "Deleted contact successfully!");
	    return "redirect:/admin";
	}
}
