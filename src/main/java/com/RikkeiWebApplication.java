package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.context.SecurityContextHolder;


@SpringBootApplication
public class RikkeiWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(RikkeiWebApplication.class, args);
	}
	

}
